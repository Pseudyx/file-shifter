﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Base;

namespace FileShifterUI
{
    public partial class FileShifterUI : Form
    {
        private string ConfigFile;
        private XmlDocument oXml;
        List<ShiftPoint> shiftPoints;
        List<Notification> Notifications;

        public FileShifterUI()
        {
            InitializeComponent();
            LoadConfig();
        }

        private void LoadConfig()
        {
            try
            {
                string ConfigName = ConfigurationManager.AppSettings["ServiceConfig"];
                string AppPath = AppDomain.CurrentDomain.BaseDirectory;
                ConfigFile = (string)Path.Combine(AppPath, ConfigName);     

                if (File.Exists(ConfigFile) == false)
                {
                    MessageBox.Show(ConfigFile);
                    return;
                }

                oXml = new XmlDocument();
                oXml.Load(ConfigFile);

                XmlNodeList SPList = oXml.GetElementsByTagName("ShiftPoint");
                shiftPoints = new List<ShiftPoint>();

                foreach (XmlNode point in SPList)
                {
                    ShiftPoint shiftPoint = new ShiftPoint();
                    shiftPoint.Source = point.Attributes["source"].Value.ToString();
                    shiftPoint.Destination = point.Attributes["destination"].Value.ToString();
                    shiftPoint.ID = point.Attributes["id"].Value.ToString();
                    shiftPoints.Add(shiftPoint);
                }

                XmlNodeList NList = oXml.GetElementsByTagName("Notification");
                Notifications = new List<Notification>();

                foreach (XmlNode note in NList)
                {
                    Notification notification = new Notification();
                    notification.ID = note.Attributes["id"].Value.ToString();
                    notification.Email = note.Attributes["email"].Value.ToString();
                    notification.Name = note.Attributes["name"].Value.ToString();
                    notification.ShiftPointID = note.Attributes["shiftpointid"].Value.ToString();
                    Notifications.Add(notification);
                }

                string[] emptyPoints = shiftPoints.Where(x => !Notifications.Select(n => n.ShiftPointID).Contains(x.ID)).Select(x => x.ID).ToArray();
                foreach (string id in emptyPoints)
                {
                    Notification notification = new Notification();
                    notification.ID = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                    notification.Email = null;
                    notification.Name = null;
                    notification.ShiftPointID = id;
                    Notifications.Add(notification);
                }

                DataSet dataSet = new DataSet();
                dataSet.Tables.Add(shiftPoints.ToDataTable());
                dataSet.Tables.Add(Notifications.ToDataTable());

                DataColumn keyColumn = dataSet.Tables[0].Columns["ID"];
                DataColumn foreignKeyColumn = dataSet.Tables[1].Columns["ShiftPointID"];
                dataSet.Relations.Add("ShiftPointNotifications", keyColumn, foreignKeyColumn);

                ShiftPointGrid.DataSource = dataSet.Tables[0];
                ShiftPointGrid.ForceInitialize();

                GridView gridView = new GridView(ShiftPointGrid);
                gridView.OptionsCustomization.AllowGroup = false;
                gridView.OptionsView.ShowGroupPanel = false;
                gridView.OptionsView.NewItemRowPosition = NewItemRowPosition.Bottom;
                ShiftPointGrid.LevelTree.Nodes.Add("ShiftPointNotifications", gridView);

                gridView.ViewCaption = "Shift Point Notifications";
                gridView.PopulateColumns(dataSet.Tables[1]);
                gridView.Columns["ShiftPointID"].VisibleIndex = -1;
                gridView.Columns["ID"].VisibleIndex = -1;
                gridView.RowUpdated += new RowObjectEventHandler(gridView_RowUpdated);
                gridView.InitNewRow += new InitNewRowEventHandler(gridView_InitNewRow);
                gridView.KeyDown += new KeyEventHandler(gridView_KeyDown);

                gridView1.RowUpdated += new RowObjectEventHandler(gridView1_RowUpdated);
                gridView1.KeyDown += new KeyEventHandler(gridView1_KeyDown);

            }
            catch { }
        }

        void gridView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
            {
                if (MessageBox.Show("Delete Notification?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    return;
                GridView view = sender as GridView;
                DataRow row = view.GetDataRow(view.FocusedRowHandle);

                XmlNodeList NList = oXml.GetElementsByTagName("Notification");
                List<XmlNode> dNList = new List<XmlNode>();
                foreach (XmlNode node in NList)
                {
                    if (node.Attributes["id"].Value == row["ID"].ToString()) { dNList.Add(node); }
                }

                XmlNodeList NRoot = oXml.GetElementsByTagName("Notifications");
                foreach (XmlNode node in dNList)
                {
                    NRoot[0].RemoveChild(node);
                }

                oXml.Save(ConfigFile);
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete && e.Modifiers == Keys.Control)
            {
                if (MessageBox.Show("Delete ShiftPoint and all underlying Notifications?", "Confirmation", MessageBoxButtons.YesNo) != DialogResult.Yes)
                    return;
                GridView view = sender as GridView;
                DataRow row = view.GetDataRow(view.FocusedRowHandle);
                
                XmlNodeList NList = oXml.GetElementsByTagName("Notification");
                List<XmlNode> dNList = new List<XmlNode>();
                foreach (XmlNode node in NList)
                {
                    if (node.Attributes["shiftpointid"].Value == row["ID"].ToString()){ dNList.Add(node); }
                }
                
                XmlNodeList SList = oXml.GetElementsByTagName("ShiftPoint");
                List<XmlNode> dSPList = new List<XmlNode>();
                foreach (XmlNode node in SList)
                {
                    if (node.Attributes["id"].Value == row["ID"].ToString()){ dSPList.Add(node); }
                }

                XmlNodeList NRoot = oXml.GetElementsByTagName("Notifications");
                foreach (XmlNode node in dNList)
                {
                    NRoot[0].RemoveChild(node);
                }

                XmlNodeList SPRoot = oXml.GetElementsByTagName("ShiftPoints");
                foreach (XmlNode node in dSPList)
                {
                    SPRoot[0].RemoveChild(node);
                }

                oXml.Save(ConfigFile);
                view.DeleteRow(view.FocusedRowHandle);
            }
        }

        void gridView1_RowUpdated(object sender, RowObjectEventArgs e)
        {
            DataRowView view = (DataRowView)e.Row;
            DataRow row = (DataRow)view.Row;

            XmlNodeList SPList = oXml.GetElementsByTagName("ShiftPoint");

            if (shiftPoints.Select(x => x.ID).Contains((row.ItemArray)[2].ToString()))
            {
                foreach (XmlNode node in SPList)
                {
                    if (node.Attributes["id"].Value == (row.ItemArray)[2].ToString())
                    {
                        node.Attributes["source"].Value = (row.ItemArray)[0].ToString();
                        node.Attributes["destination"].Value = (row.ItemArray)[1].ToString();
                    }
                }
            }
            else
            {
                XmlNode node = oXml.CreateElement("ShiftPoint");
                XmlAttribute id = oXml.CreateAttribute("id");
                id.Value = (row.ItemArray)[2].ToString();
                node.Attributes.Append(id);
                XmlAttribute source = oXml.CreateAttribute("source");
                source.Value = (row.ItemArray)[0].ToString();
                node.Attributes.Append(source);
                XmlAttribute destination = oXml.CreateAttribute("destination");
                destination.Value = (row.ItemArray)[1].ToString();
                node.Attributes.Append(destination);

                XmlNodeList SPRoot = oXml.GetElementsByTagName("ShiftPoints");
                SPRoot[0].AppendChild(node);
            }
            oXml.Save(ConfigFile);
            LoadConfig();
        }

        void gridView_InitNewRow(object sender, InitNewRowEventArgs e)
        {
            ColumnView View = sender as ColumnView;
            string seq = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            View.SetRowCellValue(e.RowHandle, View.Columns["ID"], seq);
        }

        void gridView_RowUpdated(object sender, RowObjectEventArgs e)
        {
            DataRowView view = (DataRowView)e.Row;
            DataRow row = (DataRow)view.Row;

            XmlNodeList NList = oXml.GetElementsByTagName("Notification");
            Notifications.Clear();

            foreach (XmlNode note in NList)
            {
                Notification notification = new Notification();
                notification.ID = note.Attributes["id"].Value.ToString();
                notification.Email = note.Attributes["email"].Value.ToString();
                notification.Name = note.Attributes["name"].Value.ToString();
                notification.ShiftPointID = note.Attributes["shiftpointid"].Value.ToString();
                Notifications.Add(notification);
            }

            if (Notifications.Select(x => x.ID).Contains((row.ItemArray)[0].ToString()))
            {
                foreach (XmlNode node in NList)
                {
                    if (node.Attributes["id"].Value == (row.ItemArray)[0].ToString())
                    {
                        node.Attributes["name"].Value = (row.ItemArray)[1].ToString();
                        node.Attributes["email"].Value = (row.ItemArray)[2].ToString();
                    }
                }
            }
            else
            {
                XmlNode node = oXml.CreateElement("Notification");
                XmlAttribute id = oXml.CreateAttribute("id");
                id.Value = (row.ItemArray)[0].ToString();
                node.Attributes.Append(id);
                XmlAttribute name = oXml.CreateAttribute("name");
                name.Value = (row.ItemArray)[1].ToString();
                node.Attributes.Append(name);
                XmlAttribute email = oXml.CreateAttribute("email");
                email.Value = (row.ItemArray)[2].ToString();
                node.Attributes.Append(email);
                XmlAttribute shiftpoint = oXml.CreateAttribute("shiftpointid");
                shiftpoint.Value = (row.ItemArray)[3].ToString();
                node.Attributes.Append(shiftpoint);

                XmlNodeList NRoot = oXml.GetElementsByTagName("Notifications");
                NRoot[0].AppendChild(node);
            }
            oXml.Save(ConfigFile);
            
        }

        
    }

    public class ShiftPoint 
    {
        private string cSource;
        private string cDestination;
        private string cId;

        public String Source
        {
            get { return cSource; }
            set { cSource = value; }
        }

        
        public String Destination
        {
            get { return cDestination; }
            set { cDestination = value; }
        }

        
        public String ID
        {
            get { return cId; }
            set { cId = value; }
        }
    }

    public class Notification 
    {
        private string cId;
        private string cName;
        private string cEmail;
        private string cShiftPointID;

        public String ID
        {
            get { return cId; }
            set { cId = value; }
        }

        public String Name
        {
            get { return cName; }
            set { cName = value; }
        }

        public String Email
        {
            get { return cEmail; }
            set { cEmail = value; }
        }

        public String ShiftPointID
        {
            get { return cShiftPointID; }
            set { cShiftPointID = value; }
        }
    }

    public static class Extensions
    {
        public static DataTable ToDataTable<T>(this IList<T> list)
        {
            if (list == null || list.Count == 0) { return null; }

            Type elementType = typeof(T);
            DataTable table = new DataTable();
            foreach (var propInfo in elementType.GetProperties())
            {
                table.Columns.Add(propInfo.Name, Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType);
            }

            foreach (T item in list)
            {
                DataRow row = table.NewRow();
                foreach (var propInfo in elementType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }
            return table;
        }
    }
}
