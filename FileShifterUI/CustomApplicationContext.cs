﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Reflection;
using System.ServiceProcess;

namespace FileShifterUI
{
    public class CustomApplicationContext : ApplicationContext
    {
        private static readonly string IconFileName = "folder_wrench.ico";
        private static readonly string DefaultTooltip = "FileShifter";

        public CustomApplicationContext()
        {
            InitializeContext();
        }

        #region generic code framework

        private System.ComponentModel.IContainer components;	// a list of components to dispose when the context is disposed
        private NotifyIcon SysTrayIcon;				            // the icon that sits in the system tray
        private ServiceController controller;

        private void InitializeContext()
        {
            components = new System.ComponentModel.Container();
            SysTrayIcon = new NotifyIcon(components)
            {
                ContextMenuStrip = new ContextMenuStrip(),
                Icon = new Icon(IconFileName),
                Text = DefaultTooltip,
                Visible = true
            };
            SysTrayIcon.ContextMenuStrip.Opening += ContextMenuStrip_Opening;
            SysTrayIcon.MouseUp += SysTrayIcon_MouseUp;

            try{
               controller = new ServiceController("FileShifter");
            } catch{}
        }

        /// <summary>
        /// When the application context is disposed, dispose things like the notify icon.
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null) { components.Dispose(); }
        }

        /// <summary>
        /// When the exit menu item is clicked, make a call to terminate the ApplicationContext.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitItem_Click(object sender, EventArgs e)
        {
            ExitThread();
        }

        /// <summary>
        /// If we are presently showing a form, clean it up.
        /// </summary>
        protected override void ExitThreadCore()
        {
            // before we exit, let forms clean themselves up.
            //if (introForm != null) { introForm.Close(); }
            //if (detailsForm != null) { detailsForm.Close(); }

            SysTrayIcon.Visible = false; // should remove lingering tray icon
            base.ExitThreadCore();
        }


        public ToolStripMenuItem ToolStripMenuItemWithHandler(string displayText, EventHandler eventHandler)
        {
            var item = new ToolStripMenuItem(displayText);
            if (eventHandler != null) { item.Click += eventHandler; }

            return item;
        }

        #endregion generic code framework

        #region Working Forms
        private FileShifterUI fileShifterForm;

        private void SysTrayIcon_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                MethodInfo mi = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
                mi.Invoke(SysTrayIcon, null);
            }
        }

        private void ShowConfigForm()
        {
            if (fileShifterForm == null)
            {
                fileShifterForm = new FileShifterUI();
                fileShifterForm.Closed += fileShifterForm_Closed; // avoid reshowing a disposed form
                fileShifterForm.Show();
            }
            else { fileShifterForm.Activate(); }
        }

        // attach to context menu items
        private void showConfigItem_Click(object sender, EventArgs e) { ShowConfigForm(); }
        private void StartService_Click(object sender, EventArgs e) { try { controller.Start(); ContextMenuStrip_Opening(SysTrayIcon.ContextMenuStrip, null); } catch { } }
        private void StopService_Click(object sender, EventArgs e) { try { controller.Stop(); ContextMenuStrip_Opening(SysTrayIcon.ContextMenuStrip, null); } catch { } }
        private void Error_Click(object sender, EventArgs e) { MessageBox.Show("Error: Service Not installed"); }

        // null out the forms so we know to create a new one.
        private void fileShifterForm_Closed(object sender, EventArgs e) { fileShifterForm = null; }

        public void BuildContextMenu(ContextMenuStrip contextMenuStrip)
        {
            try{
                ServiceControllerStatus Status = controller.Status;
                if(Status == ServiceControllerStatus.Running || Status == ServiceControllerStatus.ContinuePending){
                    contextMenuStrip.Items.Add(ToolStripMenuItemWithHandler("S&top Service", StopService_Click));
                } 
                else 
                {
                    contextMenuStrip.Items.Add(ToolStripMenuItemWithHandler("&Start Service", StartService_Click));
                }
            } catch {
                contextMenuStrip.Items.Add(ToolStripMenuItemWithHandler("Error", Error_Click));
            }
        }

        #endregion

        private void ContextMenuStrip_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = false;
            SysTrayIcon.ContextMenuStrip.Items.Clear();
            BuildContextMenu(SysTrayIcon.ContextMenuStrip);
            SysTrayIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            SysTrayIcon.ContextMenuStrip.Items.Add(ToolStripMenuItemWithHandler("Show &Config", showConfigItem_Click));
            SysTrayIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());
            SysTrayIcon.ContextMenuStrip.Items.Add(ToolStripMenuItemWithHandler("&Exit", exitItem_Click));
        }

    }
}
