﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using FileShifter.Engine.Processor;

namespace FileShifter.Engine.Scheduler
{
    public class SystemScheduler
    {
        FileWatcherScheduler fileWatcherScheduler;
        public volatile Boolean Abort = false;

        /// <summary>
        /// Starts the following processes
        /// 
        /// </summary>
        public void Run()
        {
            fileWatcherScheduler = new FileWatcherScheduler(this);
        }

        /// <summary>
        /// Sets abort variable so inherited threads know to stop
        /// </summary>
        public void Stop()
        {
            Abort = true;
            while (!fileWatcherScheduler.Finished) Thread.Sleep(100);
            fileWatcherScheduler.Stop();
        }
    }

    abstract class BaseScheduler
    {
        protected SystemScheduler scheduler;
        Thread thread;

        public volatile bool Finished = false;

        public BaseScheduler(SystemScheduler scheduler)
        {
            this.scheduler = scheduler;

            thread = new Thread(Execute);
            thread.Start();
        }

        void Execute()
        {
            try { Run(); }
            finally { Finished = true; }
        }

        protected abstract void Run();
    }
}
