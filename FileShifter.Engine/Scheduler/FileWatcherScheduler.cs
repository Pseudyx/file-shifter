﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using FileShifter.Core;
using FileShifter.Core.Sys;
using FileShifter.Engine.Processor;
using System.Configuration;

namespace FileShifter.Engine.Scheduler
{
    class FileWatcherScheduler : BaseScheduler
    {
        public FileWatcherScheduler(SystemScheduler scheduler) : base(scheduler) { }
        public List<Thread> WatcherThreadList;

        protected override void Run()
        {
            using (var system = new XSystem())
            {
                system.ProcessName = "FileProcessorScheduler";
                
                try
                {
                    system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Scheduler, String.Format("{0} started", system.ProcessName));

                    var config = ShiftPointsConfig.GetConfig();
                    int id = 0;

                    WatcherThreadList = new List<Thread>();
                    foreach (var itm in config.ShiftPoints)
                    {
                        string protocol = (((ShiftPoint)itm).Protocol != null) ? ((ShiftPoint)itm).Protocol.ToLower() : "filesystem";

                        Thread WatcherThread;
                        id++;

                        switch (protocol)
                        {
                            default:
                            case "filesystem":
                                using (var fileWatcher = new FileWatcher(system, ((ShiftPoint)itm).Source, ((ShiftPoint)itm).Destination, ((ShiftPoint)itm).ID))
                                {
                                    WatcherThread = new Thread(new ThreadStart(fileWatcher.Execute));
                                    WatcherThread.IsBackground = true;
                                    WatcherThread.Name = "FileShifterWatcher_" + id.ToString();
                                    WatcherThread.Start();
                                }
                                break;
                            case "sftp":
                            case "ftp":
                                using (var remoteWatcher = new RemoteWatcher(system, scheduler, ((ShiftPoint)itm).Source, ((ShiftPoint)itm).Destination, ((ShiftPoint)itm).ID,
                                                                                                ((ShiftPoint)itm).Protocol, ((ShiftPoint)itm).UserName, ((ShiftPoint)itm).Password,
                                                                                                ((ShiftPoint)itm).Port, ((ShiftPoint)itm).Host))
                                {
                                    WatcherThread = new Thread(new ThreadStart(remoteWatcher.Execute));
                                    WatcherThread.IsBackground = true;
                                    WatcherThread.Name = "FileShifterWatcher_" + id.ToString();
                                    WatcherThread.Start();
                                }
                                break;
                        }
                        WatcherThreadList.Add(WatcherThread);

                    }                    

                }
                catch (Exception ex) { system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, ex.Message); }
                finally
                {
                    Finished = true; //tell the ControllerService that current cycle has finished, so service can be stopped
                    //system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Scheduler, String.Format("{0} stopped ", system.ProcessName));
                }
            }
        }

        public void Stop()
        {
            base.scheduler.Abort = true;

            if (WatcherThreadList != null)
            {
                foreach (Thread WatcherThread in WatcherThreadList)
                {
                    if (WatcherThread != null)
                    {
                        if (WatcherThread.IsAlive)
                        {
                            WatcherThread.Abort();
                        }
                    }
                }
            }
        }
    }
}
