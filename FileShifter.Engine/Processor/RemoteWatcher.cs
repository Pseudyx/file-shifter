﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using FileShifter.Core;
using FileShifter.Core.Data;
using FileShifter.Core.Utils;
using FileShifter.Core.Sys;
using FileShifter.Engine.Utils;
using FileShifter.Engine.Scheduler;

namespace FileShifter.Engine.Processor
{
    public class RemoteWatcher : Disposable
    {
        XSystem system;
        SystemScheduler scheduler;

        IList<Thread> threads = new List<Thread>();
        String sourceDir;
        String destinationDir;
        String ShiftPointID;
        String protocol;
        String username;
        String password;
        String host;
        int port;

        int ftpSessions = 0;
        int maxFTPSessions = 1;
        public volatile bool Abort = false;

        public RemoteWatcher(XSystem System, SystemScheduler Scheduler, String Source, String Destination, String ID, String Protocol, String UserName, String Password, String Port, String Host)
        {
            system = System;
            scheduler = Scheduler;
            system.ProcessName = "RemoteWatcher";
            Abort = scheduler.Abort;

            sourceDir = Source;
            destinationDir = Destination;
            ShiftPointID = ID;
            protocol = Protocol;
            username = UserName;
            password = Password;
            host = Host;
            int.TryParse(Port, out this.port);
        }

        protected override void DisposeManaged()
        {
            system.Dispose();
        }

        /// <summary>
        /// Execute FTP watcher and open connection in Thread
        /// </summary>
        public void Execute()
        {
            try
            {
                system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Processor, String.Format("{0} executed", system.ProcessName));

                while (!scheduler.Abort)
                {
                    if (maxFTPSessions > ftpSessions)
                    {
                        FTPThread p = new FTPThread(this.system, this, this.protocol, this.host, this.username, this.password, this.port, this.sourceDir, this.destinationDir, this.ShiftPointID);
                        while (!p.Started) Thread.Sleep(10000); //wait 10 seconds 
                    }
                    if (!scheduler.Abort) Thread.Sleep(1000);
                }


            }
            catch (Exception ex)
            {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Processor, ex.Message);
                return;
            }
        }

        public void RegisterFTPSession() { Interlocked.Increment(ref ftpSessions); }
        public void UnregisterFTPSession() { Interlocked.Decrement(ref ftpSessions); }


    }

    public class FTPThread
    {
        public Boolean Started { get; private set; }

        int lastPercent = -1;
        XSystem system;
        RemoteWatcher processor;
        Thread thread;
        
        FtpBaseClient client;
        String source;
        String destination;
        String protocol;
        String host;
        String username;
        String password;
        int port;

        String shiftPointID;

        public FTPThread(XSystem System, RemoteWatcher Processor, String Protocol, String Host, String UserName, String Password, int Port, String Source, String Destination, String ShiftPointID)
        {
            system = System;
            processor = Processor;
            source = Source;
            destination = Destination;
            protocol = Protocol;
            host = Host;
            username = UserName;
            password = Password;
            port = Port;

            shiftPointID = ShiftPointID;

            thread = new Thread(DoFTP);
            thread.Start();
        }

        void Progress(float percentage)
        {
            if (processor.Abort)
            {
                client.Disconnect();
            }
            else
            {
                int thisPerc = Convert.ToInt32(percentage);
                if (lastPercent != thisPerc)
                {
                    lastPercent = thisPerc;
                }
            }
        }

        /// <summary>
        /// Main Method to send the FTP/SFTP 
        /// </summary>
        public void DoFTP()
        {
            try
            {
                using (XSystem system = new XSystem())
                {
                        processor.RegisterFTPSession();
                        try
                        {
                            system.ProcessName = "FTP/sFTP";
                            Started = true; 

                            using (client = FtpClient.GetClient(this.protocol))
                            {
                                try
                                {
                                    client.Connect(this.host, this.port, this.username, this.password);
                                    client.Progress += Progress;

                                    List<string> fileList = new List<string>();

                                    if (client.Connected)
                                    {
                                        foreach (string Filename in client.ListFiles(this.source))
                                        {
                                            string file = Path.Combine(this.source, Filename);
                                            client.Download(file, this.destination, Filename);
                                            fileList.Add(Filename);
                                        }
                                    }
                                    //Still connected?
                                    if (client.Connected)
                                    {
                                        foreach (string filename in fileList)
                                        {
                                            //send notifications
                                            SendNotification(shiftPointID, this.source, this.destination, filename, true);

                                            //Delete remote File
                                            string file = Path.Combine(this.source, filename);
                                            client.DeleteFile(file);
                                        }
                                    }
                                    else
                                    {
                                        system.Logger.SystemLog("Process", true, LogErrorID.Unexpected, string.Format("{0} aborted", system.ProcessName));
                                    }
                                    client.Disconnect();

                                }
                                catch (Exception ex)
                                {
                                    client.Disconnect();
                                //    system.SendNotificationEmail(message, ex.ToString());
                                    system.Logger.SystemLog("Process", false, LogErrorID.Unexpected, string.Format("{2} exception downloading {0} to {1}", host, destination, system.ProcessName));
                                }
                            }
                        }
                        catch (Exception ex) { system.Logger.SystemLog("FTPThread", false, LogErrorID.Unexpected, "Unexpected exception"); }
                        finally { processor.UnregisterFTPSession(); }
                    
                }
            }
            catch { }
        }

        private bool SendNotification(string ID, string sourcePath, string targetPath, string fileName, bool fileMoved)
        {
            BaseMessage xmessage = new BaseMessage(system);

            bool success;
            try
            {
                List<Notification> Notifications = new List<Notification>();
                var config = NotificationsConfig.GetConfig();
                foreach (var itm in config.Notifications)
                {
                    Notifications.Add((Notification)itm);
                }

                List<Notification> SendList = Notifications.Where(x => x.ShiftPointID == ID).ToList();
                foreach (Notification contact in SendList)
                {
                    XMessage message = new XMessage();
                    message.ToEmail = contact.Email;
                    message.ToName = contact.Name;

                    StringBuilder HtmlMessage = new StringBuilder();

                    String MessageHeader = string.Format(@"<!DOCTYPE html>
                            <html>
                                <head>
                                    <style type=""text/css"">*{{ font-family: Arial; font-size: 10pt; }}</style>
                                </head>
                                <body>File Recieved: {0}<br/><br/>", fileName);
                    HtmlMessage.Append(MessageHeader);
                    String MessageTableTop = @"<table border=""0"" cellpadding=""0"" cellspacing=""0"">
                            <tr>
                                <td>";
                    HtmlMessage.Append(MessageTableTop);

                    String MessageInfo = string.Format(@"Source Folder: {0}<br/>
                                                     Destination Folder: {1}<br/>
                                                         File Moved: {2}<br/>", sourcePath, targetPath, fileMoved);
                    HtmlMessage.Append(MessageInfo);

                    if (!fileMoved)
                    {
                        String MessageAdmin = @"<br/><b>Error when moving File. Please Contact System Adminstrator";
                        HtmlMessage.Append(MessageAdmin);
                    }
                    else
                    {
                        HtmlMessage.Append("= = = = = = = = = =");
                        HtmlMessage.AppendLine();
                        FileInfo srcFile = new FileInfo(Path.Combine(targetPath, fileName));
                        using (StreamReader fileStream = new StreamReader(srcFile.OpenRead())){
                            String LogDetails = fileStream.ReadToEnd();
                            HtmlMessage.Append(LogDetails);
                            HtmlMessage.AppendLine();
                        }
                        HtmlMessage.Append("= = = = = = = = = =");
                        HtmlMessage.AppendLine();
                    }

                    String MessageTableBtm = @"
                                </td>
                            </tr>
                        </table><br /><br />";
                    HtmlMessage.Append(MessageTableBtm);

                    String MessageFooter = "<br/><br/><b>This message was automatically generated by FileShifter Service.</b><br/></body></html>";
                    HtmlMessage.Append(MessageFooter);

                    message.MessageBody = HtmlMessage.ToString();
                    message.Subject = string.Format("File Recieved: {0}", fileName);

                    xmessage.SendMessage(message);
                }

                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, ex.Message);
            }

            return success;
        }
    }


}
