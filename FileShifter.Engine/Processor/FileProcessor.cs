﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

using FileShifter.Core;
using FileShifter.Core.Utils;
using FileShifter.Core.Data;
using FileShifter.Core.Sys;


namespace FileShifter.Engine.Processor
{
    public class FileProcessor : BaseMessage
    {
        XSystem system;

        public FileProcessor(XSystem System)
            : base(System)
        {
            system = System;
            system.ProcessName = "FileProcessor";
        }

        protected override void DisposeManaged()
        {
            system.Dispose();
        }

        public void Execute(Object obj)
        {
            FileParameters prms = (FileParameters)obj;
            string ID = prms.ID;
            string inputFolder = prms.InputFolder;
            string outputFolder = prms.OutputFolder;
            FileInfo srcFile = prms.SrcFileName;

            try
            {
                bool fileMoved = MoveFile(inputFolder, outputFolder, srcFile.Name);
                SendNotification(ID, inputFolder, outputFolder, srcFile.Name, fileMoved);
            }
            catch(Exception ex) {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, ex.Message);
            }
            
        }

        private bool MoveFile(string sourcePath, string targetPath, string fileName)
        {
            bool success;
            try
            {
                if (CheckDirectory(targetPath))
                {
                    string sourceFile = Path.Combine(sourcePath, fileName);
                    string destinationFile = Path.Combine(targetPath, fileName);

                    File.Move(sourceFile, destinationFile);
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            catch
            {
                success = false;
            }
            return success;
        }
        private bool CheckDirectory(string path)
        {
            bool result = false;
            try
            {
                DirectoryInfo info = new DirectoryInfo(path);
                if (!info.Exists)
                {
                    info.Create();
                }

                result = info.Exists;
            }
            catch (Exception ex)
            {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Processor, ex.Message);
                result = false;
            }
            return result;
        }
        private bool SendNotification(string ID, string sourcePath, string targetPath, string fileName, bool fileMoved)
        {
            bool success;
            try
            {
                List<Notification> Notifications = new List<Notification>();
                var config = NotificationsConfig.GetConfig();
                foreach (var itm in config.Notifications)
                {
                    Notifications.Add((Notification)itm);
                }
                
                List<Notification> SendList = Notifications.Where(x => x.ShiftPointID == ID).ToList();
                foreach (Notification contact in SendList)
                {
                    XMessage message = new XMessage();
                    message.ToEmail = contact.Email;
                    message.ToName = contact.Name;

                    StringBuilder HtmlMessage = new StringBuilder();

                    String MessageHeader = string.Format(@"<!DOCTYPE html>
                            <html>
                                <head>
                                    <style type=""text/css"">*{{ font-family: Arial; font-size: 10pt; }}</style>
                                </head>
                                <body>File Recieved: {0}<br/><br/>", fileName);
                    HtmlMessage.Append(MessageHeader);
                    String MessageTableTop = @"<table border=""0"" cellpadding=""0"" cellspacing=""0"">
                            <tr>
                                <td>";
                    HtmlMessage.Append(MessageTableTop);

                    String MessageInfo = string.Format(@"Source Folder: {0}<br/>
                                                     Destination Folder: {1}<br/>
                                                         File Moved: {2}<br/>", sourcePath, targetPath, fileMoved);
                    HtmlMessage.Append(MessageInfo);

                    if (!fileMoved)
                    {
                        String MessageAdmin = @"<br/><b>Error when moving File. Please Contact System Adminstrator";
                        HtmlMessage.Append(MessageAdmin);
                    }

                    String MessageTableBtm = @"
                                </td>
                            </tr>
                        </table><br /><br />";
                    HtmlMessage.Append(MessageTableBtm);

                    String MessageFooter = "<br/><br/><b>This message was automatically generated by FileShifter Service.</b><br/></body></html>";
                    HtmlMessage.Append(MessageFooter);

                    message.MessageBody = HtmlMessage.ToString();
                    message.Subject = string.Format("File Recieved: {0}", fileName);

                    SendMessage(message);
                }

                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, ex.Message);
            }

            return success;
        }
        
        
    }

    public class FileParameters
    {
        private string inputFolder = null;
        private string outputFolder = null;
        private FileInfo srcFileName = null;
        private string id = null;

        public FileParameters(
                string inputFolder,
                string outputFolder,
                FileInfo srcFileName,
                string id
            )
        {
            this.inputFolder = inputFolder;
            this.outputFolder = outputFolder;
            this.srcFileName = srcFileName;
            this.id = id;
        }

        public FileInfo SrcFileName
        {
            get { return srcFileName; }
        }

        public string InputFolder
        {
            get { return inputFolder; }
        }

        public string OutputFolder
        {
            get { return outputFolder; }
        }

        public string ID
        {
            get { return id; }
        }
    }
}
