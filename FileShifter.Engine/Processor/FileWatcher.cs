﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using FileShifter.Core.Utils;
using FileShifter.Core.Sys;

namespace FileShifter.Engine.Processor
{
    public class FileWatcher : Disposable
    {
        XSystem system;
        FileSystemWatcher fileSysWatcher;
        IList<Thread> threads = new List<Thread>();
        String sourceDir;
        String destinationDir;
        String ShiftPointID;

        public FileWatcher(XSystem System, String Source, String Destination, String ID)
        {
            system = System;
            system.ProcessName = "FileWatcher";
            sourceDir = Source;
            destinationDir = Destination;
            ShiftPointID = ID;
        }

        protected override void DisposeManaged()
        {
            system.Dispose();
        }

        public void Execute()
        {
            try
            {
                system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Processor, String.Format("{0} executed", system.ProcessName));

                fileSysWatcher = new FileSystemWatcher(sourceDir);

                fileSysWatcher.InternalBufferSize = 64000;

                fileSysWatcher.Filter = "*.*";

                fileSysWatcher.Created += new FileSystemEventHandler(fileSysWatcher_Event);
                fileSysWatcher.Error += new ErrorEventHandler(fileSysWatcher_Error);
                fileSysWatcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.CreationTime;
                fileSysWatcher.EnableRaisingEvents = true;


            }
            catch (Exception ex)
            {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Processor, ex.Message);
                return;
            }
        }

        private void fileSysWatcher_Event(object sender, FileSystemEventArgs e)
        {
            FileInfo file = new FileInfo(Path.Combine(sourceDir, e.Name.ToString()));

            Thread.Sleep(2500);// wait and give a chance to the file creation process to finish writing file
            while (Fx.IsFileLocked(file))
            {
                Thread.Sleep(2500); // wait and give a chance for the file to unlock
            }

            try
            {
                system.Logger.SystemLog(system.ProcessName, true, LogErrorID.Processor, String.Format("{0} event captured", system.ProcessName));

                //Execute FileProcessor to process file
                using (var fileProcessor = new FileProcessor(system))
                {
                    FileParameters prms = new FileParameters(sourceDir, destinationDir, file, ShiftPointID);

                    int tCount = (threads != null) ? threads.Count : 0;
                    Thread thread = new Thread(new ParameterizedThreadStart(fileProcessor.Execute));
                    thread.IsBackground = true;
                    thread.Name = "FileProcessor " + tCount.ToString();
                    thread.Start(prms);

                    threads.Add(thread);
                }
            }
            catch (Exception ex)
            {
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, ex.Message);
            }
        }

        private void fileSysWatcher_Error(object sender, ErrorEventArgs e)
        {
            Exception ex = e.GetException();

            if (ex != null)
            {
                string msg = "Error raised by FileSystemWatcher. Error message " + ex.Message;
                system.Logger.SystemLog(system.ProcessName, false, LogErrorID.Unexpected, msg);
            }
        }
    }
}
