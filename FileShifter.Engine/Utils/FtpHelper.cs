﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atp.IO;
using Atp.Net;
using FileShifter.Core.Utils;

namespace FileShifter.Engine.Utils
{
    public abstract class FtpBaseClient : Disposable
    {
        public event Action<float> Progress;
        public bool Connected { get; protected set; }

        protected abstract void DoConnect(string host, int port, string usename, string password);
        protected abstract void DoDisconnect();
        public abstract void Upload(string source, string target);
        public abstract void Download(string source, string target, string Filename);
        public abstract void DeleteFile(string FileName);
        public abstract string[] ListFiles(string remoteDir = null);

        public void Connect(string host, int port, string usename, string password)
        {
            DoConnect(host, port, usename, password);
            Connected = true;
        }

        public void Disconnect()
        {
            if (Connected)
            {
                try { DoDisconnect(); }
                finally { Connected = false; }
            }
        }

        protected void client_Progress(object sender, FileSystemProgressEventArgs e)
        {
            if (Progress != null)
                Progress(e.Percentage);
        }

        public static FtpBaseClient GetClient(string protocol)
        {
            if ("FTP".Equals(protocol, StringComparison.CurrentCultureIgnoreCase)) return new FtpClient();
            else if ("SFTP".Equals(protocol, StringComparison.CurrentCultureIgnoreCase)) return new SftpClient();
            else return null;
        }
    }

    public class FtpClient : FtpBaseClient
    {
        Ftp client;

        protected override void DisposeManaged()
        {
            client.Dispose();
        }

        protected override void DoConnect(string host, int port, string usename, string password)
        {
            client = new Ftp();
            client.Connect(host, port);
            client.Authenticate(usename, password);
            client.Progress += new FileSystemProgressEventHandler(client_Progress);
        }

        protected override void DoDisconnect()
        {
            client.Disconnect();
        }

        public override void Upload(string source, string target)
        {
            client.UploadFile(source, target);
        }

        public override void Download(string source, string target, string Filename)
        {
            string localFile = FileSystemPath.Combine(target, Filename);
            client.DownloadFile(source, localFile);
        }

        public override void DeleteFile(string FileName)
        {
            client.DeleteFile(FileName);
        }

        public override string[] ListFiles(string remoteDir = null)
        {
            if (remoteDir != null)
            {
                return client.ListName(remoteDir);
            }
            else
            {
                return client.ListName();
            }
        }
        
    }


    public class SftpClient : FtpBaseClient
    {
        Sftp client;

        protected override void DisposeManaged()
        {
            client.Dispose();
        }

        protected override void DoConnect(string host, int port, string usename, string password)
        {
            client = new Sftp();
            client.Connect(host, port);
            client.Authenticate(usename, password);
            client.Progress += new FileSystemProgressEventHandler(client_Progress);
        }

        protected override void DoDisconnect()
        {
            client.Disconnect();
        }

        public override void Upload(string source, string target)
        {
            client.UploadFile(source, target);
        }

        public override void Download(string source, string target, string Filename)
        {
            string localFile = FileSystemPath.Combine(target, Filename);
            client.DownloadFile(source, localFile);
        }

        public override void DeleteFile(string FileName)
        {
            client.DeleteFile(FileName);
        }

        public override string[] ListFiles(string remoteDir = null)
        {
            if (remoteDir != null)
            {
                return client.ListName(remoteDir);
            }
            else
            {
                return client.ListName();
            }
        }
        

    }
}
