﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace FileShifter.Core
{
    public class Notification : ConfigurationElement
    {
        [ConfigurationProperty("id", IsRequired = true)]
        public String ID
        {
            get
            {
                return this["id"] as string;
            }
        }

        [ConfigurationProperty("name", IsRequired = true)]
        public String Name
        {
            get
            {
                return this["name"] as string;
            }
        }

        [ConfigurationProperty("email", IsRequired = true)]
        public String Email
        {
            get
            {
                return this["email"] as string;
            }
        }

        [ConfigurationProperty("shiftpointid", IsRequired = true)]
        public String ShiftPointID
        {
            get
            {
                return this["shiftpointid"] as string;
            }
        }
    }

    public class Notifications : ConfigurationElementCollection
    {
        public Notification this[int index]
        {
            get
            {
                return base.BaseGet(index) as Notification;
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new Notification this[string responseString]
        {
            get { return (Notification)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Notification();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Notification)element).Email;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "Notification"; }
        }
    }

    public class NotificationsConfig : ConfigurationSection
    {
        public static NotificationsConfig GetConfig()
        {
            return (NotificationsConfig)ConfigurationManager.GetSection("NotificationsConfig") ?? new NotificationsConfig();
        }

        [ConfigurationProperty("Notifications")]
        [ConfigurationCollection(typeof(Notifications), AddItemName = "Notification")]
        public Notifications Notifications
        {
            get
            {
                return (Notifications)this["Notifications"];
            }
        }
    }
}
