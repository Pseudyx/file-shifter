﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace FileShifter.Core.Utils
{
    public class Fx
    {
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            bool value = false;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                value = true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return value;
        }
        public static bool IsEmpty(object obj)
        {
            bool result = false;

            if (obj == null || obj == DBNull.Value)
                result = true;
            else if (string.IsNullOrEmpty(((string)obj).Trim()))
                result = true;

            return result;
        }

        public static DataSet ImportCSVFile(Stream stream, string tableName, char delimiter, bool headersInFirstRow, bool addFileInfo, int colNumber)
        {
            DataSet ds = null;
            int lineCnt = 0;
            string line = null;

            UtilsExt utilsExt = new UtilsExt();
            utilsExt.delimiter = delimiter;
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);

            while (sr.Peek() >= 0)
            {
                line = sr.ReadLine();
                lineCnt++;

                if (colNumber > 0)
                {
                    int rowColCnt = GetNumberOfFields(line, delimiter);

                    if (rowColCnt + 1 < colNumber)
                    {
                        do
                        {
                            line += sr.ReadLine();

                            rowColCnt = GetNumberOfFields(line, delimiter);
                        }
                        while (rowColCnt + 1 < colNumber);
                    }
                }

                if (lineCnt == 1)
                {
                    ds = utilsExt.CreateDataSet(line, tableName, headersInFirstRow, addFileInfo);

                    if (colNumber <= 0)
                        colNumber = ds.Tables[tableName].Columns.Count;

                    if (headersInFirstRow == false)
                        utilsExt.AddRowToDataSet(ds, line, lineCnt);
                }
                else
                {
                    utilsExt.AddRowToDataSet(ds, line, lineCnt);
                }
            }

            return ds;
        }
        public static DataSet ImportCSVFile(Stream stream, string tableName, char delimiter, bool headersInFirstRow, bool addFileInfo)
        {
            DataSet ds = null;
            int lineCnt = 0;
            string line = null;

            UtilsExt utilsExt = new UtilsExt();
            utilsExt.delimiter = delimiter;
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);

            while (sr.Peek() >= 0)
            {
                line = sr.ReadLine();
                lineCnt++;

                if (lineCnt == 1)
                {
                    ds = utilsExt.CreateDataSet(line, tableName, headersInFirstRow, addFileInfo);

                    if (headersInFirstRow == false)
                    {
                        if (addFileInfo) line += "," + tableName + "," + lineCnt.ToString();
                        utilsExt.AddRowToDataSet(ds, line, lineCnt);
                    }
                }
                else
                {
                    if (addFileInfo) line += "," + tableName + "," + lineCnt.ToString();
                    utilsExt.AddRowToDataSet(ds, line, lineCnt);
                }
            }

            return ds;
        }
        private static int GetNumberOfFields(string line, char delimiter)
        {
            int result = 0;

            int pos = line.IndexOf(delimiter);

            if (pos >= 0)
            {
                do
                {
                    result++;
                    pos = line.IndexOf(delimiter, pos + 1);
                }
                while (pos >= 0);
            }

            return result;
        }
    }

    internal class UtilsExt
    {
        private static int nextPosInLine = 0;
        private string colName = null;
        internal char delimiter = ',';

        internal DataSet CreateDataSet(string line, string tableName, bool headersInFirstRow, bool addFileInfo)
        {
            Type typeString = Type.GetType("System.String");
            DataSet ds = new DataSet();
            ds.Tables.Add(tableName);

            List<string> list = line.Split(new char[] { delimiter }).ToList();
            if (addFileInfo)
            {
                list.Add("FileName");
                list.Add("FileRowNum");
            }
            string[] fields = list.ToArray();



            for (int i1 = 0; i1 < fields.Length; i1++)
            {
                if (headersInFirstRow)
                    colName = GetColValue(fields[i1]);
                else
                    colName = "_col" + i1.ToString();

                DataColumn dc = new DataColumn(colName, typeString);
                ds.Tables[0].Columns.Add(dc);
            }

            return ds;
        }

        private string GetColValue(string val)
        {
            string result = null;

            if (val != null && val.Length > 1 && val.StartsWith("\"") && val.EndsWith("\""))
            {
                if (val.Length == 2)
                    result = "";
                else
                    result = val.Substring(1, val.Length - 2);
            }
            else
                result = val;

            return result;
        }

        internal void AddRowToDataSet(DataSet ds, string line, int lineCnt)
        {
            string value = null;
            int filedCnt = 0;
            DataRow dr = ds.Tables[0].NewRow();
            Object obj = null;
            int colCnt = ds.Tables[0].Columns.Count;

            value = GetFirstToken(line);

            while (value != null)
            {
                if (string.IsNullOrEmpty(value))
                    obj = null;
                else
                    obj = GetColValue(value);

                if (filedCnt < colCnt)
                    dr[filedCnt] = obj;
                else
                    throw new ApplicationException(string.Format("CSV file has invalid structure line no {0}", lineCnt));

                value = GetNextToken(line);
                filedCnt++;
            }

            ds.Tables[0].Rows.Add(dr);
        }

        // extract first token from a line
        private string GetFirstToken(string line)
        {
            nextPosInLine = 0;

            return GetToken(line);
        }

        // extract next token from the line or null the end of the line has bee reached
        private string GetNextToken(string line)
        {
            return GetToken(line);
        }

        // extract token from the posintion stored in nextPosInLine field
        private string GetToken(string line)
        {
            string result = null;

            if (nextPosInLine < line.Length)
            {
                bool openquota = false;
                char ch;
                bool cont = true;
                result = "";

                while (cont)
                {
                    if (nextPosInLine < line.Length)
                    {
                        ch = line[nextPosInLine++];

                        if (ch == '"')
                            openquota = !openquota;

                        if (ch != delimiter || openquota)
                            result += ch;

                        // end of token
                        if (ch == delimiter && openquota == false)
                            cont = false;
                    }
                    else
                        cont = false;
                }
            }

            return result;
        }
    }
}
