﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FileShifter.Core.Sys
{
    public class XMessage
    {
        private string cToEmail;
        private string cToName;
        private string cSubject;
        private string cMessageBody;

        public string ToEmail
        {
            get { return cToEmail; }
            set { cToEmail = value; }
        }
        public string ToName
        {
            get { return cToName; }
            set { cToName = value; }
        }
        public string Subject
        {
            get { return cSubject; }
            set { cSubject = value; }
        }
        public string MessageBody
        {
            get { return cMessageBody; }
            set { cMessageBody = value; }
        }
    }
}
