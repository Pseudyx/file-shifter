﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace FileShifter.Core
{
    public class ShiftPoint : ConfigurationElement
    {
        [ConfigurationProperty("source", IsRequired = true, IsKey=false)]
        public String Source
        {
            get
            {
                return this["source"] as string;
            }
        }

        [ConfigurationProperty("destination", IsRequired = true, IsKey = false)]
        public String Destination
        {
            get
            {
                return this["destination"] as string;
            }
        }

        [ConfigurationProperty("id", IsRequired = true, IsKey = true)]
        public String ID
        {
            get
            {
                return this["id"] as string;
            }
        }

        [ConfigurationProperty("protocol", IsRequired = false, IsKey = true)]
        public String Protocol
        {
            get
            {
                return this["protocol"] as string;
            }
        }

        [ConfigurationProperty("host", IsRequired = false, IsKey = true)]
        public String Host
        {
            get
            {
                return this["host"] as string;
            }
        }

        [ConfigurationProperty("username", IsRequired = false, IsKey = true)]
        public String UserName
        {
            get
            {
                return this["username"] as string;
            }
        }

        [ConfigurationProperty("password", IsRequired = false, IsKey = true)]
        public String Password
        {
            get
            {
                return this["password"] as string;
            }
        }

        [ConfigurationProperty("port", IsRequired = false, IsKey = true)]
        public String Port
        {
            get
            {
                return this["port"] as string;
            }
        }
    }

    public class ShiftPoints : ConfigurationElementCollection
    {
        public ShiftPoint this[int index]
        {
            get
            {
                return (ShiftPoint)base.BaseGet(index);
            }
            set
            {
                if (base.BaseGet(index) != null)
                {
                    base.BaseRemoveAt(index);
                }
                this.BaseAdd(index, value);
            }
        }

        public new ShiftPoint this[string responseString]
        {
            get { return (ShiftPoint)BaseGet(responseString); }
            set
            {
                if (BaseGet(responseString) != null)
                {
                    BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
                }
                BaseAdd(value);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ShiftPoint();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ShiftPoint)element).ID;
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override string ElementName
        {
            get { return "ShiftPoint"; }
        }
    }

    public class ShiftPointsConfig : ConfigurationSection
    {
        public static ShiftPointsConfig GetConfig()
        {
            return (ShiftPointsConfig)ConfigurationManager.GetSection("ShiftPointsConfig") ?? new ShiftPointsConfig();
        }

        [ConfigurationProperty("ShiftPoints")]
        [ConfigurationCollection(typeof(ShiftPoints), AddItemName = "ShiftPoint")]
        public ShiftPoints ShiftPoints
        {
            get
            {
                return (ShiftPoints)base["ShiftPoints"];
            }
        }
    }
}
