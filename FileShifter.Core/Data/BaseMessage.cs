﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using FileShifter.Core.Utils;
using FileShifter.Core.Sys;

namespace FileShifter.Core.Data
{
    public class BaseMessage : Disposable
    {
        protected MessageStore mq;
        public XSystem System { get; private set; }

        public BaseMessage(XSystem system)
        {
            System = system;
            mq = new MessageStore(XConfiguration.MessageConnection);
        }

        public bool SendMessage(XMessage message)
        {
            bool result;
            try
            {
                if (mq == null)
                {
                    try { mq = new MessageStore(XConfiguration.MessageConnection); }
                    catch (Exception ex) { Log(ex, LogErrorID.Configuration, String.Format("Creating EF MessageQueue exception: {0}", ex.Message)); }
                }

                Email email = mq.Emails.Where(x => x.Address == message.ToEmail).FirstOrDefault();
                if (email == null)
                {
                    email = mq.Emails.CreateObject();
                    email.Address = message.ToEmail;
                    email.Name = message.ToName;

                    mq.Emails.AddObject(email);
                    mq.SaveChanges();

                    mq.CheckAndSetEmailRecipient((int)email.EmailId);
                }


                MessageQueue MSG = mq.MessageQueues.CreateObject();

                MSG.Protocol = "Email";
                MSG.Priority = 10;
                MSG.Received = DateTime.Now;
                MSG.Status = "Pending";
                MSG.Subject = message.Subject;
                MSG.Body = message.MessageBody;
                MSG.Sender_EmailId = 35; //notifications@select-solutions.com.au
                MSG.InsertedBy_AuthID = 9; //EvoOfflineFileProcessor
                MSG.Changedby_AuthID = 9; //EvoOfflineFileProcessor
                mq.MessageQueues.AddObject(MSG);

                Queue_Recipient Recipient = mq.Queue_Recipient.CreateObject();
                Recipient.MessageQueue = MSG;
                Recipient.EmailId = email.EmailId;

                mq.Queue_Recipient.AddObject(Recipient);

                mq.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                Log(ex, LogErrorID.DataUpdate, string.Format("Error during MessageQueue update: '{0}'", ex.Message));
                Close();
                result = false;
            }
            finally
            {
                Close();
            }

            return result;
        }

        protected void Close()
        {
            if (mq != null)
            {
                try { mq.Dispose(); }
                catch { /* no need to log */ }
                finally { mq = null; }
            }
        }

        protected virtual void Log(bool positive, int errorID, string comment)
        {
            EventLogger.Post(positive, errorID, comment);
        }

        protected virtual void Log(Exception ex, int errorID, string comment)
        {
            EventLogger.Post(ex, errorID, comment);
        }

        protected override void DisposeManaged()
        {
            Close();
        }
    }
}
