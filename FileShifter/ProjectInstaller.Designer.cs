﻿namespace FileShifter
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FileShifterServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.FileShifterServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // FileShifterServiceProcessInstaller
            // 
            this.FileShifterServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.FileShifterServiceProcessInstaller.Password = null;
            this.FileShifterServiceProcessInstaller.Username = null;
            // 
            // FileShifterServiceInstaller
            // 
            this.FileShifterServiceInstaller.Description = "FileShifter";
            this.FileShifterServiceInstaller.DisplayName = "FileShifter";
            this.FileShifterServiceInstaller.ServiceName = "FileShifter";
            this.FileShifterServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.FileShifterServiceProcessInstaller,
            this.FileShifterServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller FileShifterServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller FileShifterServiceInstaller;
    }
}