﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.IO;

namespace FileShifter
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();

            var inst = FindEventLogInstaller(Installers);
            if (inst != null)
                inst.Log = "FileShifter";
        }

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
            ServiceController controller = new ServiceController("FileShifter");

            try
            {
                controller.Start();
            }
            catch (Exception ex)
            {
                String source = "FileShifter";
                String log = "FileShifter";

                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, log);
                };

                EventLog eLog = new EventLog();
                eLog.Source = source;
                eLog.WriteEntry(@"The service could not be started. Please start the service manually. Error: " + ex.Message, EventLogEntryType.Error);
            }
           
            try
            {

                string AppPath = Context.Parameters["assemblypath"].Replace("FileShifter.exe",""); 
                string ExeName = (string)Path.Combine(AppPath, "FileShifterUI.exe");

                if (File.Exists(ExeName) != false)
                {
                    Process.Start(ExeName);
                }

            }
            catch (Exception ex)
            {
                String source = "FileShifter";
                String log = "FileShifter";

                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, log);
                };

                EventLog eLog = new EventLog();
                eLog.Source = source;
                eLog.WriteEntry(ex.Message, EventLogEntryType.Error);
                try { eLog.WriteEntry(ex.Message, EventLogEntryType.Error); }
                catch { }
            }
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }

        private EventLogInstaller FindEventLogInstaller(InstallerCollection installers)
        {
            foreach (Installer inst in installers)
            {
                if (inst is EventLogInstaller)
                    return (EventLogInstaller)inst;

                if (inst.Installers != null)
                {
                    EventLogInstaller instLog = FindEventLogInstaller(inst.Installers);
                    if (instLog != null)
                        return instLog;
                };
            };

            return null;
        }
    }
}
